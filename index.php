<?php 
/**
 * Plugin Name: Simple Plug-In
 * Description: Plugin allows you to use a shortcode [use_me] to create a post from the page or post.
 * Author: Leshka
 * Version: 1.0.0
 */

 if(!function_exists('add_action')) {
     echo "ERROR";
     exit;
 }
 
 // SETUP

 //INCLUDES

 include('include/enqueue.php');
 include('include/myplugin_func.php');
 include('include/admin.php');

 //HOOKS, ACTIONS

 add_action('wp_enqueue_scripts','myplugin_styles');
 add_action('admin_enqueue_scripts', 'admin_myplugin_styles');
 add_action('admin_menu', 'admin_mail_settings');
 add_action('admin_init', 'my_plugin_register_options');

 //SHORTCODES

 add_shortcode('use_me', 'myplugin_func');

