<?php


$form_template = '
<div class="postForm_wrapper">

<form action="' . get_the_permalink() . '" id="postForm" method="post">

 <label for="title">Enter the title for your new post</label>

    <input type="text" name="title" id="title" required>

    <label for="text">Write the text for your new post</label>

    <textarea name="text" id="text" rows="5" cols="30"></textarea>

    <input type="submit" name="submit" class="myplugin__submit" value="Post it!">
</form></div>';

