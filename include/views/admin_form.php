
  <div class="wrap my_plugin__admin-wrapper">
  <h2>SMTP mailing settings</h2>
  <form method="post" action="options.php" class="my_plugin__admin-wrapper">
  <?php
            settings_fields( 'myplugin_fields' );
            do_settings_sections( 'myplugin_fields' );
        ?>
    <div class="form-group">
    <label for="my_plugin_host">SMTP Host: </label>
    <input name="my_plugin_host" class="form-control form-control-lg" id="my_plugin_host" value="<?php echo get_option('my_plugin_host'); ?>" type="text">
    </div>
    <div class="form-group">
    <label for="my_plugin_port">SMTP Port: </label>
    <input name="my_plugin_port" class="form-control form-control-lg" type="text" value="<?php echo get_option('my_plugin_port'); ?>">
    </div>
    <div class="form-group">
    <label for="my_plugin_username">Username: </label>
    <input name="my_plugin_username" class="form-control form-control-lg" type="text" value="<?php echo get_option('my_plugin_username'); ?>">
    </div>
    <div class="form-group">
    <label for="my_plugin_password">Password: </label>
    <input name="my_plugin_password" class="form-control form-control-lg" type="text" value="<?php echo get_option('my_plugin_password'); ?>">
    </div>
    <?php $checked = get_option('my_plugin_secure'); ?>
    <div class="form-check">
    <input class="form-check-input my_plugin-radio" type="radio" name="my_plugin_secure" id="my_plugin_ssl" value="ssl" <?php if($checked == 'ssl'){ echo 'checked';} ?>>
        <label class="form-check-label my_plugin-radio__label" for="my_plugin_ssl">
            SSL
        </label>
    </div>
    <div class="form-check">
        <input class="form-check-input my_plugin-radio" type="radio" name="my_plugin_secure" id="my_plugin_tls" value="tls" <?php if($checked == 'tls'){ echo 'checked';} ?>>
        <label class="form-check-label my_plugin-radio__label" for="my_plugin_tls">
            TLS
        </label>
    </div>

    <input type="submit" name="submit" class="btn btn-danger btn-lg btn-block" value="Confirm">
  </form>
</div>