<?php

// Adding a CSS styles to the plugin for admin form

function admin_myplugin_styles() {
    wp_enqueue_style( 'admin_style', plugins_url( '../css/admin-style.css' , __FILE__ ) );
    wp_enqueue_style( 'bootstrap_style', plugins_url( '../css/bootstrap.min.css' , __FILE__ ) );
}

// Adding a new panel for main Wordpress menu

function admin_mail_settings() {

    add_menu_page(
        'My Plug-In',
        'My Plug-In Options',
        'manage_options',
        'myplugin_fields',
        'my_plugin_html_page',
        '',
        20
    );
}

// Including an admin HTML form template

function my_plugin_html_page() {
  require_once 'views/admin_form.php';
}

// Registering settings from the form above

function my_plugin_register_options() {
  register_setting('myplugin_fields', 'my_plugin_host');
  register_setting('myplugin_fields', 'my_plugin_port');
  register_setting('myplugin_fields', 'my_plugin_username');
  register_setting('myplugin_fields', 'my_plugin_password');
  register_setting('myplugin_fields', 'my_plugin_secure');
}


