<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Main function

function myplugin_func() {
    $le_is_sent = FALSE;
    $le_is_post_exist = FALSE;
    $le_posts = get_posts(array(
        'post_status' => 'any'
    ));

    include('views/form.php'); // Including HTML form template to the main function

    if (isset($_POST['submit'])) {
        $title = trim($_POST['title']);
        $text = trim($_POST['text']);
    
        if(!empty($title)) {
        
            foreach ($le_posts as $post) {
                if ($post->post_title == $title) {
                    echo "<script>alert('This post title is already exist!')</script>";
                    $le_is_post_exist = true;
                    unset($_POST['submit']);
                    break;
                }
            }    

        }
        
        // Create post object
        if (!$le_is_post_exist) {
            $new_post = array(
                'post_title'    => $title,
                'post_content'  => $text,
                'post_status'   => 'draft',
                'post_author'   => 1
            );
            
            // Insert the post into the database
            $wp_my_data = wp_insert_post( $new_post );
            $le_is_sent = TRUE;

        $mail = new PHPMailer(true);

        //Server settings
        $mail->SMTPDebug = 0;                    //Enable verbose debug output
        $mail->isSMTP();                                            //Send using SMTP
        $mail->Host       = get_option('my_plugin_host');                     //Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
        $mail->Username   = get_option('my_plugin_username');                     //SMTP username
        $mail->Password   = get_option('my_plugin_password');                     //SMTP password
        if(get_option('my_plugin_secure') == 'ssl') {
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;  
        }
        else {
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;  
        }                                
        $mail->Port       = get_option('my_plugin_port');                                
        
        //Recipients
        $mail->setFrom(get_option('my_plugin_username'), 'Mailer');
        $mail->addAddress(get_option('admin_email'));            
        
        //Content
        $mail->isHTML(true);                                  //Set email format to HTML
        $mail->Subject = 'Mail from MyPlug-In ';
        $mail->Body    = 'Hey! My Plug-In form created a post with title: ' . $title;
        try {
           $mail->send();
           echo '<script>alert("Message sent")</script>';
        } catch(\Exception $exception) {
            echo '<script>alert("Something went wrong");</script> ' . $exception->getMessage();
        }

         }
     }

     // Success message
    
    if ($le_is_sent) {
        $le_posts = get_posts(array(
            'post_status' => 'any'
        )
        );
        echo '<div class="form-info__wrapper"><div class="form-info">
        <h3>Form successfully sended!</h3>
        <p>Your newly created post: <a href="' . $le_posts[0]->guid .'">link</a></p>
    </div></div>';
    unset($_POST);
    }
    else {
        echo $form_template;
    }
    
}